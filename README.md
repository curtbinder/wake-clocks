# wake-clocks
DIY Ok to Wake clock on raspberry pi zero with Blinkt LED lights

## run-lights.py
Installed on pi in USER/bin folder.
It reads nap-schedule.json to determine what color to display on the lights.
Need to setup cronjob to run the script every minute

## nap-schedule.json
Installed on pi in /var/www/html folder.
Contains the times for the lights.

## post-light.php
Installed on pi in /var/www/html folder.
Reads json file sent from android app/client to update the nap-schedule.json file

## light.php
Installed on pi in /var/www/html folder.
Reads nap-schedule.json file and sends it to the requesting android app/client.


Made from: 
 * raspberry pi zero w
 * blinkt add-on hat
