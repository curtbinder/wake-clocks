#!/bin/sh

# Gus
echo "Deploying to zero-gus"
scp run-lights.py zero-gus:~/bin/
ssh zero-gus 'chmod +x ~/bin/run-lights.py'
#ssh zero-gus 'sudo chown pi /var/www/html/nap-schedule.json'
#scp nap-schedule-gus.json zero-gus:/var/www/html/nap-schedule.json
#ssh zero-gus 'sudo chown www-data /var/www/html/nap-schedule.json'
scp light.php zero-gus:/var/www/html/
scp post-light.php zero-gus:/var/www/html/
ssh zero-gus 'sudo chgrp www-data /var/www/html/*.php'

echo ""

# Emmett
echo "Deploying to zero-emmett"
scp run-lights.py zero-emmett:~/bin/
ssh zero-emmett 'chmod +x ~/bin/run-lights.py'
#ssh zero-emmett 'sudo chown pi /var/www/html/nap-schedule.json'
#scp nap-schedule-emmett.json zero-emmett:/var/www/html/nap-schedule.json
#ssh zero-emmett 'sudo chown www-data /var/www/html/nap-schedule.json'
scp light.php zero-emmett:/var/www/html/
scp post-light.php zero-emmett:/var/www/html/
ssh zero-emmett 'sudo chgrp www-data /var/www/html/*.php'
