<?php
# get JSON as a string
$json_str = file_get_contents('php://input');

# write the JSON string to file
$fp = fopen('nap-schedule.json', 'w');
fwrite($fp, $json_str);
fclose($fp);

echo '{ "success": true }';
?>
